/*
 * GLWidget.h
 *
 *  Created on: Feb 8, 2014
 *      Author: ruben
 */

#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QGLWidget>
#include <iostream>
#include <functional>
#include <vector>
#include <array>
#include <cmath>

#include <QWheelEvent>
#include <QGesture>
#include <QGLBuffer>

using namespace std;

class GLWidget: public QGLWidget
{
	Q_OBJECT
public:
	GLWidget(QGLContext* context, QWidget* parent);
	virtual ~GLWidget();

protected:
	virtual void initializeGL();	// Called once
	virtual void resizeGL(int w, int h);	// Called every time the window resizes (incl upon init)
	virtual void paintGL();		// Called for painting. Can be forced with this->update(). NEVER call directly

	/**
	 * Catches a mouse wheel event to zoom in
	 */
	void wheelEvent(QWheelEvent* event);

	bool event(QEvent* event);

	void mouseReleaseEvent(QMouseEvent * event);

	void mouseMoveEvent(QMouseEvent * event);

	void mousePressEvent(QMouseEvent * event);

	void keyPressEvent(QKeyEvent * event);


private:
	bool m_isMouseMoving;

	QPoint m_lastMousePosition;

	QPoint m_totalRotation;

//	double m_zAngle;

	double m_xAngle;

	double m_yAngle;


	// This is in pref in vleaf
	array<double, 3> m_offset;

	double m_magnification;

	double m_outline_width;


};

#endif // GLWIDGET_H
