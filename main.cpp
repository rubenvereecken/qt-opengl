/*
 * main.cpp
 *
 *  Created on: Jan 5, 2014
 *      Author: ruben
 */

#include <iostream>
#include <QMainWindow>
#include <QApplication>

#include <QGLFormat>
#include <QGLContext>

#include "GLWidget.h"

using namespace std;

int main(int argc, char *argv[])
{
	std::cout << "Hello world." << std::endl;
	QApplication a(argc, argv);
	/*
	 QMainWindow w;
	 w.show();
	 */
	QGLFormat glFormat;
	glFormat.setVersion(3, 1);
	glFormat.setProfile(QGLFormat::CompatibilityProfile); // Requires >=Qt-4.8.0
	glFormat.setSampleBuffers(true);

	QGLContext* glContext = new QGLContext(glFormat);
	glContext->create();

	// Create a GLWidget requesting our format
	GLWidget w(glContext, nullptr);
	w.show();

	return a.exec();
}

