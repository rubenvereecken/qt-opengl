/*
 * GLWidget.cpp
 *
 *  Created on: Feb 8, 2014
 *      Author: ruben
 */

#include "GLWidget.h"

GLWidget::GLWidget(QGLContext* context, QWidget* parent)
	: QGLWidget(context, parent), m_magnification(10.), m_totalRotation(15, 15)
{
}

GLWidget::~GLWidget()
{
	// TODO Auto-generated destructor stub
}

void GLWidget::initializeGL()
{
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glShadeModel (GL_SMOOTH);

	GLfloat mat_shininess[] = { 128 };	// I believe 128 is the shiniest
	GLfloat light_position[] = { 0., 0., -1.0, 0.0 };
	GLfloat white_light[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat blue_light[] = { 0., 0., 1., 1.};
	GLfloat lmodel_ambient[] = { .3, .3, .3, 1. };
	GLfloat black_light[] = {0., 0., 0., 1.0};

	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);// Shiny shiny shiny
	glLightfv(GL_LIGHT0, GL_POSITION, light_position); 	// This position should be elsewhere really..
	glLightfv(GL_LIGHT0, GL_DIFFUSE, white_light);
	glLightfv(GL_LIGHT0, GL_SPECULAR, white_light); 	// So no specular really
	glLightfv(GL_LIGHT0, GL_AMBIENT, lmodel_ambient);

	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);

	glEnable (GL_LINE_SMOOTH);	// Antialiasing. Qt doesn't support I think
	glEnable (GL_LIGHTING);
	glEnable (GL_LIGHT0);
	glEnable (GL_DEPTH_TEST);
}

/**
 * Simulate the vleaf perspective. Positive Y direction is toward the bottom of the screen.
 * Center is center of the screen.
 */
void GLWidget::resizeGL(int w, int h)
{
	glViewport(0, 0, (GLsizei) w, (GLsizei) h);
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-((GLdouble) w) / 2, ((GLdouble) w) / 2, ((GLdouble) h) / 2, -((GLdouble) h) / 2, -1.0, 1.0);
	//glFrustum(...) if we want a realistic projection
	glMatrixMode (GL_MODELVIEW);
}

void square(float r, float g, float b) {
	GLfloat color[] = {r, g, b, 1.};
	glMaterialfv(GL_FRONT, GL_DIFFUSE, color);
	    glTranslatef(0,0,0.5);    // Move square 0.5 units forward.
	    glBegin(GL_TRIANGLE_FAN);
	    glVertex2f(-0.5,-0.5);    // Draw the square (before the
	    glVertex2f(0.5,-0.5);     //   the translation is applied)
	    glVertex2f(0.5,0.5);      //   on the xy-plane, with its
	    glVertex2f(-0.5,0.5);     //   at (0,0,0).
	    glEnd();
}

void GLWidget::paintGL()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLineWidth(m_outline_width);

	//glLoadIdentity(); // Current matrix is now I
//	glTranslatef(m_offset[0], m_offset[1], m_offset[2]);

	glEnable(GL_NORMALIZE);

	glNormal3f(0, 0, -1);	// Specifies the normal for all points to come

	glLoadIdentity();     // start with no transformation 
	glScalef(m_magnification, m_magnification, 1.0);

	glRotatef(m_totalRotation.x() / 2., 0, 1, 0); // user rotation
	glRotatef(m_totalRotation.y() / 2., -1, 0, 0); // user rotation
	
	//GLfloat mat_specular[] = {1., 1., 1., 1.};
	//glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);

	GLfloat red[] = {1.,0,0, 1.};
	GLfloat green[] = {0,1.,0, 1.};
	GLfloat blue[] = {0,0,1., 1.};

	glMaterialfv(GL_FRONT, GL_DIFFUSE, red);
	glBegin(GL_LINES);
	glVertex3f(0, 0, 0);
	glVertex3f(100000, 0, 0);
	glEnd();
	glMaterialfv(GL_FRONT, GL_DIFFUSE, green);
	glBegin(GL_LINES);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 100000, 0);
	glEnd();
	glMaterialfv(GL_FRONT, GL_DIFFUSE, blue);
	glBegin(GL_LINES);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 0, 100000);
	glEnd();

	// Now, draw the six faces of the cube by drawing its six faces.
	glPushMatrix();
	square(1,0,0);        // front face is red
	glPopMatrix();

	glPushMatrix();
	glRotatef(180,0,1,0); // rotate square to back face
	square(0,1,1);        // back face is cyan
	glPopMatrix();

	glPushMatrix();
	glRotatef(-90,0,1,0); // rotate square to left face
	square(0,1,0);        // left face is green
	glPopMatrix();

	glPushMatrix();
	glRotatef(90,0,1,0); // rotate square to right face
	square(1,0,1);       // right face is magenta
	glPopMatrix();

	glPushMatrix();
	glRotatef(-90,1,0,0); // rotate square to top face
	square(0,0,1);        // top face is blue
	glPopMatrix();

	glPushMatrix();
	glRotatef(90,1,0,0); // rotate square to bottom face
	square(1,1,0);        // bottom face is yellow
	glPopMatrix();

	glFlush(); // To the screen!
}

/**
 * Zoomzoomzoom
 */
void GLWidget::wheelEvent(QWheelEvent* event)
{
	double scaleFactor = pow(2.0, -event->delta() / 480.0);
	m_magnification *= scaleFactor;
	m_outline_width *= scaleFactor;
	update();
}

/**
 * Ignore this one. Support for touch in Qt is aweful
 */
bool GLWidget::event(QEvent* event)
{
	if (event->type() == QEvent::Gesture) {
		printf("Received gesture\n");
		QGestureEvent* gestureEvent = dynamic_cast<QGestureEvent*>(event);
		QGesture* gesture = gestureEvent->gesture(Qt::PanGesture);
		if (gesture) {
			printf("pan me like on of your french girls\n");
		}
		return true;
	} else {
		event->ignore();
		return QWidget::event(event);
	}
}

void GLWidget::mouseReleaseEvent(QMouseEvent * event)
{
	m_isMouseMoving = false;
}

void GLWidget::mouseMoveEvent(QMouseEvent * event)
{
	if (m_isMouseMoving) {
		QPoint newPosition = event->pos();
		const QPoint diff = newPosition - m_lastMousePosition;
		m_lastMousePosition = newPosition;
		m_totalRotation += diff;
		update();
	}
}

void GLWidget::mousePressEvent(QMouseEvent * event)
{
	m_isMouseMoving = true;
	m_lastMousePosition = event->pos();
}

void GLWidget::keyPressEvent(QKeyEvent * event)
{
	int key = event->key();
	bool accepted = (key == Qt::Key_Up || key == Qt::Key_Down || key == Qt::Key_Left || key == Qt::Key_Right);

	if (key == Qt::Key_Up) {
		m_offset[1] -= 1;
	} else if (key == Qt::Key_Down) {
		m_offset[1] += 1;
	}
	if (key == Qt::Key_Left) {
		m_offset[0] -= 1;
	} else if (key == Qt::Key_Right) {
		m_offset[0] += 1;
	}

	if (accepted)
		update();
	else
		QWidget::keyPressEvent(event);
}
